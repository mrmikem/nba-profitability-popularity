# Popularity of NBA team vs Profitability

## Project structure
* ./doc folder contains the presentation and a pdf version of the jupyter notebook
* ./src/python/ contains the python code/notebook
* ./src/resources/data/ contains the data used by the python code
* ./src/resources/presentation/ contains the files used for the presentation
