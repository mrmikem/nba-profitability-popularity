#!/usr/bin/env python
# coding: utf-8

# # Popularity of NBA team vs their Profitability
# 
# ## Business Understanding
# 
# ### Why this analysis
# Understanding the objective & the context of an analysis is critical to help fullfil these goals. 
# * Is is an NBA team that need to decide whether they should invest more resources into Social Media?
# * Is this for an Asset Management firm looking to identify indicators for making inestment decisions in Sport Clubs?
# 
# This is an unknown.
# 
# ### What to analyze
# Define whether there is a correlation between between the popularity of NBA teams and their profitability
# 
# ### How to conduct this analysis
# 
# * Measure the popularity of NBA teams
# * Measure the profitability of NBA teams
# * Establish the correlation between both
# 
# 

# ## Data Acquisition
# 
# ### Data Sources
# 
# 2 Data Sources are required for the analysis:
# #### 1. The popularity of NBA teams
# Social Medias and in particular Twitter it a good indicator of popularity.
# We have measured the popularity of a team by using 2 features:
# * The number tweets published by each team since they created their Twitter account (ie: Their engagement on Twitter)
# * The numbers of followers for each team
# 
# These information are publicly available.
# 
# There could be more refined methods for measuring popularity on Twitter. For instance we could look at how much they are referrenced/mentioned by other users, the sentiments of these tweets and establish these measures by year. These information is accessible through [Twitter's Search API](https://developer.twitter.com/en/docs/tweets/search/overview/enterprise) under an Enterprise license. That data could be used at a later stage. Also other Social Media such as Instagram or Twitter could be included in the popularity analyisis.  
# The Free API of Twitter enables to extract only 7 days of history which wasn't relevant for our Case Study.  
# 
# #### 2. The profitability of NBA teams
# 
# To measure the profitability of NBA teams we have used their *Operating Income*.
# 
# The Operating Income is a superior measure than the sales of a team’s t-shirt. For 2 reasons:
# * It includes all revenues, not just those of a single product 
# * It includes the expenses which his a key element of the profitability (Gross Income = Gross Income − Operating Expenses).
# 
# We have retrieved the Operating Income from [Forbes' NBA 2019 valuation](https://www.forbes.com/nba-valuations/list). Forbes regularly updates this publication and their history can be obtained through the [Internet Archive](https://web.archive.org/web/*/https://www.forbes.com/nba-valuations/list/)
# 
# 
# ### Data Acquisition Pipelines
# 
# Since this was a one-off analyis and these are small data set, we have collected and stored the data into CVS files. 
# 
# Twitter Data have been collected manually since for the first time we needed to identify the twitter url for each time. The data file is stored in ../resources/data/NBATwitter.csv
# Going futher, a Pyhton library (ex: http://www.tweepy.org/)vcould be used to automate the lookup twitter account details 
# 
# The profitability data was copied from the web into a CSV file: ../resources/data/ForbesNBA2019Valuation.csv
# If this task needed to be repeated, we could automate the task by using a web scrapping python library (ex: https://scrapy.org/).  
# 
# 
# ### Data Environment
# 
# Given the small size of data used and the simplicity of the analysis (correlation), the processing doesn't need to run on a clustered infrastructure. The files have been stored in Git.
# 
# This will allow anyone to run the analysis on their host after having checked out the source code with no external dependencies.
# 
# 

#  ## Data understanding
#  
#  ### Cleaning and wrangling

# In[1]:


import pandas as pd
import matplotlib.pyplot as plt


# In[5]:



profitability_file = '../resources/data/ForbesNBA2019Valuation.csv'
profitability_df = pd.read_csv(profitability_file)
profitability_df['profit_in_M'] = profitability_df['Operating Income'].str[1:-2].astype(int)
profitability_df.head()


# In[6]:



twitter_file = '../resources/data/NBATwitter.csv'
twitter_df = pd.read_csv(twitter_file)
twitter_df['nb_tweets_in_K'] = twitter_df['NbTweets'].str[:-1].astype(float)
twitter_df['nb_followers_in_M'] = twitter_df['Followers'].str[:-1].astype(float)
twitter_df.head()


# In[7]:


#aggregate
agg_df = pd.merge(twitter_df, profitability_df, on='Team')
agg_df = agg_df[["Team", "profit_in_M", "nb_tweets_in_K", "nb_followers_in_M"]]
agg_df.head(30)


# ### Exploration: basic statistics

# In[8]:


agg_df.describe()


# In[9]:


#Normalizing the data
import numpy as np
from sklearn import preprocessing
min_max_scaler = preprocessing.MinMaxScaler()
cols = ['profit_in_M','nb_tweets_in_K','nb_followers_in_M']
profit_norm = min_max_scaler.fit_transform(agg_df[cols])
df_normalized = pd.DataFrame(profit_norm, columns=cols)
df_normalized.describe()


# ## Modeling
# 
# ### Correlation Visualization

# In[10]:


ax = agg_df.plot.scatter(x='profit_in_M', y='nb_tweets_in_K', color='DarkBlue', title='Correlation: Profitability / Number of Tweets')


# In[11]:


ax = agg_df.plot.scatter(x='profit_in_M', y='nb_followers_in_M', color='DarkBlue', title='Correlation: Profitability / Number of Followers')


# ### Correlation 

# In[18]:


agg_df.corr(method='spearman')


# ### Evaluation
# 
# The correleation between popularity of NBA teams on Twitter and their profitability **is moderate** (0.44). 
# It is to be noted that the popularity is not measured by the number of tweets published, but by the number of followers.

# 
# 

# In[ ]:




